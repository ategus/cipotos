
// IMPORTANT NOTES /////////////////////////////////////////////////////////////

// - ADC2 CAN'T BE USED TOGETHER WITH WIFI, SO VIRTUALLY, ONLY ONE ADC AVAILABLE.
// - THERE'S AN INTERNAL HALL EFFECT SENSOR, IT PREVENTS TO USE D0 WHEN USED.
// - THE I2S PORT CAN BE USED TO GET REGULARLY ADC SAMPLES, AND DMA THEM TO MEMORY
// - THERE'S A LOGGING LIBRARY, WHICH MIGHT BE INTERESTING TO SAVE STATUS INFORMATION BETWEEN RESETS.

// DEBUGGING FLAGS /////////////////////////////////////////////////////////////
#define DEBUG_SERIAL

// first example with the ESP32 and the FreeRTOS

#include <stdio.h>
#include "nvs_flash.h"          // necessary for NON-VOLATILE-STORAGE (config values and so on)
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"

#include "driver/adc.h"
#include "driver/i2c.h"


// GLOBAL DEFINITIONS //////////////////////////////////////////////////////////


// PIN DEFINITION //




// GLOBAL VARIABLES, SEMAPOHERS, MAILBOXES /////////////////////////////////////



// functions //


// TASKS ///////////////////////////////////////////////////////////////////////

void task_a(){
	unsigned int t = 0;
	unsigned int t0 = 0;
	while(1){
		t0 = xTaskGetTickCount();
		printf("Task A \n");
		vTaskDelay(100);
		t = xTaskGetTickCount();
		printf("Execution times:\n");
		printf("t=%d  t0=%d\n", t, t0);
	}
}

void task_b(){
	while(1){
		unsigned int t = 0;
		unsigned int t0 = 0;
		t0 = xTaskGetTickCount();
		printf("Task B \n");
		vTaskDelay(160);
		t = xTaskGetTickCount();
		printf("Execution times:\n");
		printf("t=%d  t0=%d\n", t, t0);
	}
}

void task_c(){
	unsigned int t = 0;
	unsigned int t0 = 0;
	while(1){
		t0 = xTaskGetTickCount();
		printf("Task C \n");
		vTaskDelay(240);
		t = xTaskGetTickCount();
		printf("Execution times:\n");
		printf("t=%d  t0=%d\n", t, t0);
	}
}

#define SIZE 40
#define ROW SIZE
#define COL SIZE

void matrix_task(){
	unsigned long t = 0;		// time at the end of the job
	unsigned long t0 = 0;		// start time for the matrix task
	unsigned long dt = 0;		// execution time for the matrix_task
	unsigned long T = 0;		// period (time between two executions of matrix_task)
	unsigned long tT = 0;		// time when the second execution starts

	int i;
	double **a = (double **)pvPortMalloc(ROW * sizeof(double*));
	for (i = 0; i < ROW; i++) a[i] = (double *)pvPortMalloc(COL * sizeof(double));
	double **b = (double **)pvPortMalloc(ROW * sizeof(double*));
	for (i = 0; i < ROW; i++) b[i] = (double *)pvPortMalloc(COL * sizeof(double));
	double **c = (double **)pvPortMalloc(ROW * sizeof(double*));
	for (i = 0; i < ROW; i++) c[i] = (double *)pvPortMalloc(COL * sizeof(double));

	double sum = 0.0;
	int j, k, l;
	int randomVar = 1000000;

	for (i = 0; i < SIZE; i++) {
		for (j = 0; j < SIZE; j++) {
			a[i][j] = 1.5;
			b[i][j] = 2.6;
			for(long i = 0; i < 2000000; i++){
				randomVar = randomVar - 1;
			}
		}
	}


	while(1){
		tT = t0;
		t0 = xTaskGetTickCount();
		T = t0 - tT;

		printf("Matrix Task \n");

		printf("Time between executions  ");
		printf("%li\n",T);

		/*
		* In an embedded systems, matrix multiplication would block the CPU for a long time
		* but since this is a PC simulator we must add one additional dummy delay.
		*/
		long simulationdelay;
		for (simulationdelay = 0; simulationdelay<1000000000; simulationdelay++);
		for (i = 0; i < SIZE; i++) {
			for (j = 0; j < SIZE; j++) {
				c[i][j] = 0.0;
			}
		}

		for (i = 0; i < SIZE; i++) {
			for (j = 0; j < SIZE; j++) {
				sum = 0.0;
				for (k = 0; k < SIZE; k++) {
					for (l = 0; l<10; l++) {
						sum = sum + a[i][k] * b[k][j];
					}
				}
				c[i][j] = sum;
			}
		}
		t = xTaskGetTickCount();
		printf("Execution times:\n");
		dt = t - t0;
		printf("t=%lu  t0=%lu\n", t, t0);
		printf("dt=%lu\n", dt);


		vTaskDelay(30);
	}
}

void communication_task(){
	unsigned long t = 0;		// time at the end of the job
	unsigned long t0 = 0;		// start time for the matrix task
	unsigned long dt = 0;		// execution time for the matrix_task
	unsigned long T = 0;		// period (time between two executions of matrix_task)
	unsigned long tT = 0;		// time when the second execution starts
	while(1){
		tT = t0;
		t0 = xTaskGetTickCount();
		T = t0 - tT;
		printf("Communication Task \n");

		printf("Time between executions  ");
		printf("%li\n",T);


		printf("Sending data...\n");
		fflush(stdout);
		vTaskDelay(10);
		printf("Data sent!\n");
		fflush(stdout);
		vTaskDelay(10);

		t = xTaskGetTickCount();
		printf("Execution times:\n");
		dt = t - t0;
		printf("t=%lu  t0=%lu\n", t, t0);
		printf("dt=%lu\n", dt);
	}
}

void intensive_task(){

	while(1){
		unsigned long i = 0;
		unsigned long j = 24001;

		for(i = 0; i < 24000; i++)
			j = j-1;
			printf("intensive nr. %lu \n ", i);

		vTaskDelay(200);
	}
}




// MAIN FUNCTION ///////////////////////////////////////////////////////////////

void app_main(){

    //nvs_flash_init();   // Initializes non-volatile memory, to read/store config data
		// OLD TASKS //
    //xTaskCreate((pdTASK_CODE)task_a, (const char *)"TASK A", 2000, NULL, 4, NULL);
		//xTaskCreate((pdTASK_CODE)task_b, (const char *)"TASK B", 2000, NULL, 3, NULL);
		//xTaskCreate((pdTASK_CODE)task_c, (const char *)"TASK C", 2000, NULL, 2, NULL);
		// NEW TASKS //
		xTaskCreate((pdTASK_CODE)matrix_task, (const char *)"matrix_task", 2000, NULL, 3, NULL);
		xTaskCreate((pdTASK_CODE)communication_task, (const char *)"communication_task", 2000, NULL, 4, NULL);
		xTaskCreate((pdTASK_CODE)intensive_task, (const char *)"intensive_task", 2000, NULL, 4, NULL);

}
